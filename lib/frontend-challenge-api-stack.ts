import * as cdk from "@aws-cdk/core";
import * as frontendChallengeApiService from "../lib/frontend-challenge-api_service";

export class FrontendChallengeApiStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    new frontendChallengeApiService.FrontendChallengeApiService(
      this,
      "FrontendChallengeApi"
    );
  }
}
