import * as core from "@aws-cdk/core";
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as lambda from "@aws-cdk/aws-lambda";
import * as dynamodb from "@aws-cdk/aws-dynamodb";

export class FrontendChallengeApiService extends core.Construct {
  constructor(scope: core.Construct, id: string) {
    super(scope, id);

    const handler = new lambda.Function(this, "ApiHandler", {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: lambda.Code.fromAsset("resources"),
      handler: "api.main",
    });

    const api = new apigateway.RestApi(this, "api", {
      restApiName: "Api Service",
      description: "This service serves api data.",
      deployOptions: {
        throttlingRateLimit: 2,
        throttlingBurstLimit: 1,
      },
    });

    const table = new dynamodb.Table(this, id, {
      tableName: "users",
      billingMode: dynamodb.BillingMode.PROVISIONED,
      readCapacity: 1,
      writeCapacity: 1,
      removalPolicy: core.RemovalPolicy.DESTROY,
      partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
      sortKey: { name: "createdAt", type: dynamodb.AttributeType.NUMBER },
      pointInTimeRecovery: true,
    });
    table.grant(handler, "dynamodb:Scan", "dynamodb:PutItem");

    const usersIntegration = new apigateway.LambdaIntegration(handler, {
      requestTemplates: { "application/json": '{ "statusCode": "200" }' },
    });

    const users = api.root.addResource("users");

    users.addMethod("GET", usersIntegration);
    users.addMethod("POST", usersIntegration);
  }
}
