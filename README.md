## Useful commands (requires aws-cli, sam-cli, cdk-cli global install)

- `sam local invoke FrontendChallengeApiApiHandler3D46AA06 -e sample-events/${event_name}.json` (requires installation of sam cli; use this to test api events locally) // TODO POST testing is broken right now - in prod, req body requires JSON.parse, but not in dev
- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `cdk diff` compare deployed stack with current state
- `cdk synth` emits the synthesized CloudFormation template - do this before deploying!
- `cdk deploy` deploy this stack to your default AWS account/region
